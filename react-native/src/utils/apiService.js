import axios from 'axios';
import { API_BASE_URL, CSRF_COOKIE, SESSION_COOKIE } from 'react-native-dotenv';
import { AsyncStorage } from 'react-native';
const RCTNetworking = require('react-native/Libraries/Network/RCTNetworking');

class ApiService {
  constructor() {
    // Create axios session
    this.session = axios.create({
      baseURL: API_BASE_URL,
      withCredentials: true,
    });

    // set headers before executing every request
    this.session.interceptors.request.use(
      async (config) => {
        if (config.headers) {
          return {
            ...config,
            headers: await this._prepareReqHeaders(),
          };
        }
        return Promise.resolve(config);
      },
      (error) => {
        return Promise.reject(error);
      },
    );

    // sync cookies on response
    this.session.interceptors.response.use(
      async (response) => {
        this._clearAutoCookies();
        await this._syncCookies(response);
        return Promise.resolve(response);
      },
      (error) => {
        this._clearAutoCookies();
        return Promise.reject(error.response.data);
      },
    );
  }

  _prepareReqHeaders = async () => {
    const csrf = await this._getCookie(CSRF_COOKIE);
    const sid = await this._getCookie(SESSION_COOKIE);

    let headers = {
      'X-Requested-With': 'XMLHttpRequest',
    };
    if (csrf) {
      headers = {
        ...headers,
        'X-CSRFToken': csrf,
      };
    }
    if (sid && csrf) {
      headers = {
        ...headers,
        Cookie: `${SESSION_COOKIE}=${sid}; ${CSRF_COOKIE}=${csrf}`,
      };
    }
    return headers;
  };

  _extractCookieValue = (input, name) => {
    const value = input.match(`${name}=(.*?);`);
    return value ? value[1] : null;
  };

  _syncCookies = async (response) => {
    if ('set-cookie' in response.headers) {
      const cookies = response.headers['set-cookie'][0];
      const sessionCookie = this._extractCookieValue(cookies, SESSION_COOKIE);
      const csrfCookie = this._extractCookieValue(cookies, CSRF_COOKIE);

      if (sessionCookie) {
        await AsyncStorage.setItem(SESSION_COOKIE, sessionCookie);
      }
      if (csrfCookie) {
        await AsyncStorage.setItem(CSRF_COOKIE, csrfCookie);
      }
    }
  };

  _clearAutoCookies = () => {
    return RCTNetworking.clearCookies(() => null);
  };

  _getCookie = async (cookieName) => {
    return AsyncStorage.getItem(cookieName);
  };

  deleteAllCookies = async () => {
    return AsyncStorage.clear();
  };

  get = async (url, params = {}) => this.session.get(url, { params });

  options = async (url, params = {}) => this.session.options(url, { params });

  post = async (url, body = {}, params = {}) => {
    return this.session.post(url, body, { params });
  };

  patch = async (url, body = {}, params = {}) => {
    return this.session.patch(url, body, { params });
  };

  delete = async (url, params = {}) => {
    return this.session.delete(url, { params });
  };
}

const apiService = new ApiService();

export default apiService;
