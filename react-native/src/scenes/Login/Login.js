import React, { useState } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { TextInput, Button } from 'react-native-paper';

import apiService from '../../utils/apiService';

const Login = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const login = async () => {
    setLoading(true);
    try {
      const { data } = await apiService.post('/api/auth/login/', {
        username,
        password,
      });
      setLoading(false);
      return navigation.navigate('Home', data.user);
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.vertical}>
        <Image style={styles.logo} source={require('../../images/logo.png')} />
        <Text style={styles.caption}>Software for roofking Companies</Text>
      </View>
      <TextInput
        style={styles.input}
        mode="outlined"
        label="Username"
        placeholder="username"
        value={username}
        onChangeText={(text) => setUsername(text)}
      />
      <TextInput
        mode="outlined"
        label="Password"
        placeholder="password"
        value={password}
        secureTextEntry={true}
        onChangeText={(text) => setPassword(text)}
      />
      <Button
        style={styles.button}
        labelStyle={styles.buttonText}
        mode="contained"
        dark
        loading={loading}
        onPress={() => login()}
      >
        Login
      </Button>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    padding: 40,
    position: 'relative',
  },
  vertical: {
    marginVertical: 80,
  },
  caption: {
    fontSize: 20,
    letterSpacing: 1,
    opacity: 0.4,
  },
  logo: {
    height: 100,
    width: 300,
    resizeMode: 'contain',
  },
  input: {
    marginBottom: 20,
  },
  button: {
    marginTop: 40,
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
