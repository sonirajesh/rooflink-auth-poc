import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Avatar, TextInput, Button, Appbar } from 'react-native-paper';
import { get, truncate } from 'lodash';

import apiService from '../../utils/apiService';

const getInitials = (name) => {
  return name
    .split(' ')
    .map((n) => (n ? n[0] : ''))
    .join('');
};

const Home = ({ route: { params }, navigation }) => {
  let [user, setUser] = useState(params);
  const [loading, setLoading] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [firstName, setFirstName] = useState(user.first_name);
  const [lastName, setLastName] = useState(user.last_name);
  const [timezone, setTimezone] = useState(user.timezone);
  const [trail, setTrail] = useState([]);

  update = async () => {
    setLoading(true);
    try {
      await apiService.patch('/api/me/profile/', {
        first_name: firstName,
        last_name: lastName,
        timezone,
      });

      const { data: profile } = await apiService.get('/api/me/info/');
      const basicInfo = profile.basic_info;
      setUser({
        name: `${basicInfo.first_name.value.value} ${basicInfo.last_name.value.value}`,
        email: basicInfo.email.value.text,
        timezone: basicInfo.timezone.value.value,
        username: basicInfo.username.value.value,
      });
      setFirstName(basicInfo.first_name.value.value);
      setLastName(basicInfo.last_name.value.value);
      setTimezone(basicInfo.timezone.value.value);

      await apiService.options('/api/me/profile/');
      await apiService.options('/api/jobs/prospect/');
      await apiService.get('/api/jobs/recent/');
      await apiService.options('/api/leaderboard/month/');
      await apiService.post('/api/events/', {
        add_to_my_calendar: true,
        all_day: false,
        any_time: false,
        description: '',
        start: '2020-07-08T06:00:00.000Z',
        end: '2020-07-08T08:00:00.000Z',
        invitees: [],
        name: 'HIIII--',
        region: null,
        reminder: '',
        tags: [],
        team: null,
        timezone: 'US/Central',
      });

      setTrail([
        'FIRED: OPTIONS: /api/me/profile/',
        'FIRED: OPTIONS: /api/jobs/prospect/',
        'FIRED: GET: /api/jobs/recent/',
        'FIRED: OPTIONS: /api/leaderboard/month',
        'FIRED: POST: /api/events',
      ]);

      setEditMode(false);
    } catch (e) {
      console.log('>>>', e);
    }
    setLoading(false);
  };

  logout = async () => {
    await apiService.post('/api/auth/logout/', { confirm_logout: true });
    await apiService.deleteAllCookies();
    return navigation.navigate('Login');
  };

  renderReadMode = () => (
    <View style={styles.card}>
      <View style={[styles.row, styles.section]}>
        <Text style={[styles.label, styles.sectionLabel]}>Basic Info</Text>
        <Button mode="text" onPress={() => setEditMode(true)}>
          Edit
        </Button>
      </View>
      {[
        { label: 'Username', key: 'username' },
        { label: 'Name', key: 'name' },
        { label: 'Email', key: 'email' },
        { label: 'Timezone', key: 'timezone' },
      ].map(({ label, key }) => (
        <View style={styles.row} key={key}>
          <Text style={styles.label}>{label}: </Text>
          <Text style={styles.value}>
            {truncate(get(user, key), { length: 28 })}
          </Text>
        </View>
      ))}
    </View>
  );

  renderEditMode = () => (
    <View style={[styles.editContainer]}>
      <TextInput
        mode="outlined"
        label="First Name"
        placeholder="First Name"
        value={firstName}
        onChangeText={(text) => setFirstName(text)}
        style={styles.input}
      />
      <TextInput
        mode="outlined"
        label="Last Name"
        placeholder="Last Name"
        value={lastName}
        onChangeText={(text) => setLastName(text)}
        style={styles.input}
      />
      <TextInput
        mode="outlined"
        label="Timezone"
        placeholder="Timezone"
        value={timezone}
        onChangeText={(text) => setTimezone(text)}
        style={styles.input}
      />
      <Button
        style={styles.button}
        labelStyle={styles.buttonText}
        mode="contained"
        dark
        loading={loading}
        onPress={() => update()}
      >
        Submit
      </Button>
      <Button
        style={styles.button}
        mode="text"
        dark
        loading={loading}
        onPress={() => setEditMode(false)}
      >
        Cancel
      </Button>
    </View>
  );

  return (
    <View>
      <View style={styles.container}>
        <Avatar.Text size={120} label={getInitials(user.name)} />
        <Button
          style={styles.button}
          mode="text"
          dark
          loading={loading}
          onPress={() => logout()}
        >
          logout
        </Button>
        {editMode ? this.renderEditMode() : this.renderReadMode()}
      </View>
      {trail.map((t) => (
        <Text key={t}>{t}</Text>
      ))}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    marginTop: 0,
    paddingVertical: 40,
    alignItems: 'center',
  },
  editContainer: {
    alignSelf: 'stretch',
    padding: 40,
  },
  card: {
    marginTop: 40,
    paddingHorizontal: 40,
    alignSelf: 'stretch',
    backgroundColor: '#efebeb',
  },
  row: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  section: {
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
  },
  sectionLabel: {
    fontWeight: 'bold',
    letterSpacing: 1,
    width: 120,
  },
  label: {
    fontSize: 20,
    opacity: 0.75,
    width: 100,
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    marginBottom: 20,
  },
  button: {
    marginTop: 10,
    height: 52,
    borderRadius: 26,
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
