module.exports = {
  extends: ['plugin:meteor/recommended', 'airbnb'],
  plugins: ['prettier', 'react-hooks'],
  parser: 'babel-eslint',
  env: {
    browser: true,
    jest: true,
  },
  rules: {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'comma-dangle': 'off',
  },
  globals: {
    fetch: false,
  },
};
