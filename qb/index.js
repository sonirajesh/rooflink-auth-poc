import apiService from "./utils/apiService.js";

const login = async () => {
  const api = "/api/auth/login/";
  const payload = {
    username: "admin",
    password: "admin",
  };

  await apiService.options(api);
  try {
    const { data: profile } = await apiService.post(api, payload);
    console.log(profile);

    const { data: detailedProfile } = await apiService.get("/api/me/");
    console.log(detailedProfile);
  } catch (e) {
    console.log(e);
  }
};

login();
